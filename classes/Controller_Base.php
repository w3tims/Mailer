<?php

/**
 * Created by PhpStorm.
 * User: w3tims
 * Date: 20.09.2016
 * Time: 22:35
 */
Abstract Class Controller_Base
{
    protected $registry;

    function __construct($registry)
    {
        $this->registry = $registry;
    }
    abstract function index();
}

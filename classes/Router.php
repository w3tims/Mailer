<?php

class Router
{
    private $registry;
    private $path;

    function __construct($registry)
    {
        $this->registry = $registry;
    }

    function delegate()
    {
        $this->path = real_path . "controllers" . DIRSEP;
        if (!is_dir($this->path)) {
            throw new Exception ('Invalid controller path: `' . $this->path . '`');
        }
        // Анализируем путь: получаем из $_GET(route) переменные: file, controller, action, args
        $this->getController($file, $controller, $action, $args);

        // Файл доступен?  и подключаем
        if (!is_readable($file)) {
            die ('404 Not Found, no file');
        }
        include ($file);

        // Создаём экземпляр контроллера
        $class = 'Controller_' . $controller;
        $this->registry['controller'] = $controller;
        $controller = new $class($this->registry);

        // Действие доступно?
        if (is_callable(array($controller, $action)) == false) {
            die ('404 Not Found, no action, </br>controller:'. $this->registry['controller'].'</br>action:'.$action);
        }

        // Выполняем действие

        $controller->$action($args);
    }


    private function getController(&$file, &$controller, &$action, &$args)
    {
        // echo "string";
        $route = (empty($_GET['route'])) ? 'index' : $_GET['route'];

        // echo $route;
        //  $route is Full route!

        // Получаем раздельные части
        $route = trim($route, '/\\');
        $parts = explode('/', $route);

        // Находим правильный контроллер
        $cmd_path = $this->path;
        foreach ($parts as $part) {
            $full_path = $cmd_path . $part;

            // Есть ли папка с таким путём?
            if (is_dir($full_path)) {
                $cmd_path .= $part . DIRSEP;
                array_shift($parts);
                continue;
            }

            // Находим файл
            if (is_file($full_path . '.php')) {
                $controller = $part;
                array_shift($parts);
                break;
            }
        }


        if (empty($controller)) {
            $controller = 'index';
        };


        // Получаем действие
        $action = array_shift($parts);
        if (empty($action)) {
            $action = 'index';
        }

        $file = $cmd_path . $controller . '.php';
        $args = $parts;
    }


}

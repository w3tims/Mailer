<?php

Class Controller_Index Extends Controller_Base
{
    function index() {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)){
            $_POST = file_get_contents('php://input');
        }

        $post = json_decode($_POST, true);
        $post['result'] = 'server-verified';

        if (isset($post["message"])) {
            $message = $post["message"];
        } else {
            echo "post[message] is missing";
            return;
        }


        $to =  isset($post["to"]) ?
            $post["to"] :
            "w3tims@gmail.com";
        switch ($to) {
            case "anna92koval@gmail.com":
                $to = "anna92koval@gmail.com";
                break;
            case "w3tims@gmail.com":
                $to = "w3tims@gmail.com";
                break;
            default:
                $to = "w3tims@gmail.com";
                break;
        }

        $subject =  isset($post["subject"]) ?
            $post["subject"] :
            "mail-notification";
        $from =  isset($post["from"]) ?
            $post["from"] :
            "Notificator@dont-reply.com";
        $reply =  isset($post["reply"]) ?
            $post["reply"] :
            "Notificator@dont-reply.com";



        $headers =  'From: ' . $from . "\r\n" .
                    'Reply-To: ' . $reply . "\r\n" .
                    'MIME-Version: 1.0' . "\r\n" .
                    'Content-Type: text/html;
                          charset=UTF-8' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

        $message = '<html><body>' . $message . '</body></html>';

        $result = mail($to, $subject, $message, $headers);

        if(!$result) {
            echo '{"result": "Error"}';
        } else {
            echo '{"result": "Success"}';
        }
    }
}

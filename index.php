<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Methods: GET, POST');

error_reporting(E_ALL);
if (version_compare(phpversion(), '5.1.0', '<') == true) {
    die ('PHP5.1 Only');
}

if (substr($_SERVER['REQUEST_URI'], -1) !== '/') {
    $uri = $_SERVER['REQUEST_URI'] . '/';
    header('Location: ' . $uri, true, 301);
    exit;
}

//
//
//  SET site-prefix:
//
//

$prefix = 'mail';

define('DIRSEP', DIRECTORY_SEPARATOR);
$real_path = realpath(dirname(__FILE__) . DIRSEP . '..' . DIRSEP);

// place site prefix between two DIRSEPs.
$real_path = $real_path . DIRSEP . $prefix . DIRSEP;
define('real_path', $real_path);


function __autoload($class_name) {
    $filename = $class_name . '.php';
    $file =  'classes/' . $filename;
    if (file_exists($file) == false) {
        return false;
    }
    include ($file);
}

$registry = new Registry;


# Загружаем router
$router = new Router($registry);
$registry->set('router', $router);
$router->delegate();                        //uses real_path
?>
